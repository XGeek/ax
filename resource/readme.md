<div align="center"><h1 align="center">AX</h3></div>
<div align="center"><h3 align="center">Windows 操作风格的后台管理系统</h3>
</div>
<p align="center">
   <img src="https://img.shields.io/badge/AX-V1.0-green" alt="AX">
   <img src="https://img.shields.io/badge/AX-权限管理-red" alt="Gitee star">
   <img src="https://img.shields.io/badge/AX-可视化-blue" alt="Gitee fork">
</p>

#### AX后台管理系统资源目录

[GIT地址]: https://gitee.com/in-git/ax-resource.git



#### 调用方式

>服务器地址+分类+文件名



#### 服务器

```
将文件放置资源服务器内
```



#### 分类

| 分类/路径  |   备注   |
| :--------: | :------: |
| image-icon | 系统图标 |
| wallpaper  | 系统壁纸 |
|   public   | 公共资源 |

